<?php

declare(strict_types=1);

// DIC configuration
use App\Controllers;
use App\Model\Entity;
use App\Model\EntityListener;
use App\Model\Repository;
use App\Services;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container = $app->getContainer();

// view renderer
$container['renderer'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

# DOCTRINE ENTITY MANAGER
$container['em'] = function (ContainerInterface $container) {
    $settings = $container->get('settings')['doctrine'];
    $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
        $settings['meta']['entity_path'],
        $settings['meta']['auto_generate_proxies'],
        $settings['meta']['proxy_dir'],
        $settings['meta']['cache'],
        false
    );

    $em = \Doctrine\ORM\EntityManager::create($settings['connection'], $config);
    // Add custom functions
    $em->getConfiguration()->addCustomDatetimeFunction('unix_timestamp', \DoctrineExtensions\Query\Mysql\UnixTimestamp::class);
    // Register listeners
    $em->getConfiguration()->getEntityListenerResolver()->register($container->get('FileEntityListener'));
    // Return entity manager
    return $em;
};

# ENTITY LISTENERS
$container['FileEntityListener'] = function (ContainerInterface $container) {
    return new EntityListener\FileEntityListener(
        $container->get('settings')['fileFullUrl']
    );
};

# CONTROLLERS
$container['ArticleController'] = function (ContainerInterface $container) {
    return new Controllers\ArticleController(
        $container->get('ArticleRepository')
    );
};

$container['ArticleCreateController'] = function (ContainerInterface $container) {
    return new Controllers\ArticleCreateController(
        $container->get('em')
    );
};

$container['ArticlesController'] = function (ContainerInterface $container) {
    return new Controllers\ArticlesController(
        $container->get('ArticleRepository')
    );
};

$container['ArticleUpdateController'] = function (ContainerInterface $container) {
    return new Controllers\ArticleUpdateController(
        $container->get('em'),
        $container->get('ArticleRepository')
    );
};

$container['ContestController'] = function (ContainerInterface $container) {
    return new Controllers\ContestController(
        $container->get('ContestRepository'),
		$container->get('em')
    );
};

$container['ContestCreateController'] = function (ContainerInterface $container) {
    return new Controllers\ContestCreateController(
        $container->get('em')
    );
};

$container['ContestDeleteController'] = function (ContainerInterface $container) {
    return new Controllers\ContestDeleteController(
        $container->get('ContestRepository'),
        $container->get('em')
    );
};

$container['ContestResultController'] = function (ContainerInterface $container) {
	return new Controllers\ContestResultController(
		$container->get('ContestRepository')
	);
};

$container['ContestActualController'] = function (ContainerInterface $container) {
	return new Controllers\ContestActualController(
		$container->get('ContestRepository'),
		$container->get('em')

	);
};

$container['ContestsController'] = function (ContainerInterface $container) {
    return new Controllers\ContestsController(
        $container->get('ContestRepository'),
	$container->get('em')
    );
};

$container['ContestUpdateController'] = function (ContainerInterface $container) {
    return new Controllers\ContestUpdateController(
        $container->get('ContestRepository'),
        $container->get('em')
    );
};

$container['FileController'] = function (ContainerInterface $container) {
    return new Controllers\FileController(
        $container->get('FileRepository'),
        $container->get('FileService'),
        $container->get('settings')['fileDownloadUrl']
    );
};

$container['FileUploadController'] = function (ContainerInterface $container) {
    return new Controllers\FileUploadController(
        $container->get('settings')['fileFullUrl'],
        $container->get('FileService')
    );
};

$container['ModelController'] = function (ContainerInterface $container) {
    return new Controllers\ModelController(
        $container->get('ModelRepository')
    );
};

$container['ModelCreateController'] = function (ContainerInterface $container) {
    return new Controllers\ModelCreateController(
        $container->get('em')
    );
};

$container['ModelDeleteController'] = function (ContainerInterface $container) {
    return new Controllers\ModelDeleteController(
        $container->get('em'),
        $container->get('ModelRepository')
    );
};

$container['ModelsController'] = function (ContainerInterface $container) {
    return new Controllers\ModelsController(
        $container->get('ModelRepository')
    );
};

$container['ModelUpdateController'] = function (ContainerInterface $container) {
    return new Controllers\ModelUpdateController(
        $container->get('em'),
        $container->get('ModelRepository')
    );
};

$container['NotificationsController'] = function (ContainerInterface $container) {
    return new Controllers\NotificationsController(
        $container->get('NotificationRepository')
    );
};

$container['NotificationDeleteController'] = function (ContainerInterface $container) {
    return new Controllers\NotificationDeleteController(
        $container->get('em'),
        $container->get('NotificationRepository')
    );
};

$container['NotificationsRegisterController'] = function (ContainerInterface $container) {
    return new Controllers\NotificationsRegisterController(
        $container->get('DeviceRepository'),
        $container->get('em')
    );
};

$container['NotificationsSendController'] = function (ContainerInterface $container) {
    return new Controllers\NotificationsSendController(
        $container->get('em')
    );
};

# REPOSITORY
$container['ArticleRepository'] = function (ContainerInterface $container) {
    return new Repository\ArticleRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Article::class)
    );
};

$container['ContestRepository'] = function (ContainerInterface $container) {
    return new Repository\ContestRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Contest::class)
    );
};

$container['DeviceRepository'] = function (ContainerInterface $container) {
    return new Repository\DeviceRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Device::class)
    );
};

$container['FileRepository'] = function (ContainerInterface $container) {
    return new Repository\FileRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\FileEntity::class)
    );
};

$container['ModelRepository'] = function (ContainerInterface $container) {
    return new Repository\ModelRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Model::class)
    );
};

$container['NotificationRepository'] = function (ContainerInterface $container) {
    return new Repository\NotificationRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Notification::class)
    );
};

$container['RoleRepository'] = function (ContainerInterface $container) {
    return new Repository\RoleRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\RoleEntity::class)
    );
};

$container['TokenRepository'] = function (ContainerInterface $container) {
    return new Repository\TokenRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\TokenEntity::class)
    );
};

$container['UserRepository'] = function (ContainerInterface $container) {
    return new Repository\UserRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\UserEntity::class)
    );
};

# SERVICES
$container['AuthenticationService'] = function (ContainerInterface $container) {
    return new Services\AuthenticationService\AuthenticationService(
        $container->get('TokenRepository'),
        $container->get('TokenStorage')
    );
};

$container['AdminAuthorizationService'] = function (ContainerInterface $container) {
    return new Services\AuthorizationService\AdminAuthorizationService(
        $container->get('TokenStorage')
    );
};
$container['FileService'] = function (ContainerInterface $container) {
    return new Services\FileService\FileService(
        $container->get('settings')['uploadsDirectory'],
        $container->get('FileRepository')
    );
};

$container['MailService'] = function (ContainerInterface $container) {
    return new Services\MailService\MailService(
        $container->get('renderer'),
        $container->get('settings')['mail']
    );
};

$container['TokenStorage'] = function () {
    return new App\Services\TokenStorage\TokenStorage();
};
