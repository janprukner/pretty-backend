<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 01. 2019
 */

declare(strict_types=1);

namespace App\Constants;

/**
 * Class Roles
 * @package App\Constants
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class Roles
{

    const ROLE_ADMIN = 'ADMIN';
    const ROLE_USER = 'USER';
}
