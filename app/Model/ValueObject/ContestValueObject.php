<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\Contest;
use DateTime;

/**
 * Class ContestValueObject
 * @package App\Model\ValueObject
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestValueObject
{

    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $timestamp;

	/**
	 * @var integer
	 */
    public $finishOn;

    /** @var DateTime */
    public $created;

    /** @var string */
    public $status;

    /** @var string */
    public $contestType;

    /** @var string */
    public $image;

    /** @var string */
    public $question;

    /** @var */
    public $answers;

    /**
     * ContestValueObject constructor.
     * @param Contest $contest
     */
    public function __construct(Contest $contest)
    {
        $this->id = $contest->getId();
        $this->finishOn = $contest->getFinishOn();
        $this->created = $contest->getCreated();
        $this->status = $contest->getStatus();
        $this->contestType = $contest->getContestType();
        $this->image = $contest->getImage();
        $this->question = $contest->getQuestion();
        $this->timestamp = new DateTime();

        $answers = $contest->getAnswers();

        if (count($answers) !== 0){
			$answersArray = [];

			foreach ($answers as $answer) {
			    $answersArray[] = $answer->getText();
			}

			$this->answers = $answersArray;
		}
    }


}
