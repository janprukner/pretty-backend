<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Device;

/**
 * Interface DeviceRepositoryInterface
 * @package App\Model\Repository
 */
interface DeviceRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param string $token
     * @return Device
     * @throws EntityNotFoundException
     */
    public function findByToken(string $token): Device;
}
