<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 27. 02. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Notification;

/**
 * Class NotificationRepository
 * @package App\Model\Repository
 */
final class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findNotification(int $id): Notification
    {
        /** @var null|Notification $entity */
        $entity = parent::find($id);
        if (null === $entity) {
            throw new EntityNotFoundException();
        }

        return $entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllNotifications(): array
    {
        return $this->_em->createQueryBuilder()
            ->select('notification.id', 'notification.description', 'notification.page', 'notification.result')
            ->addSelect('notification.state', 'notification.title')
            ->addSelect('UNIX_TIMESTAMP(notification.updated) timestamp')
            ->from(Notification::class, 'notification')
            ->getQuery()
            ->getResult();
    }
}
