<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;

/**
 * Class RoleRepository
 * @package App\Model\Repository\Role
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{

    /**
     * @return mixed|null|object
     * @throws EntityNotFoundException
     */
    public function findDefaultRole()
    {
        if ($role = $this->findOneBy(['default' => true])) {
            return $role;
        }
        throw new EntityNotFoundException();
    }
}
