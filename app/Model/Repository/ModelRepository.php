<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Model;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class ModelRepository
 * @package App\Model\Repository
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelRepository extends BaseRepository implements ModelRepositoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function findModel(int $id): Model
    {
        try {
            $article = $this->_em->createQueryBuilder()
                ->select('model')
                ->from(Model::class, 'model')
                ->andWhere('model.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $article) {
                return $article;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }
}
