<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Contest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class ContestRepository
 * @package App\Model\Repository
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestRepository extends BaseRepository implements ContestRepositoryInterface
{
	public function __construct($em, Mapping\ClassMetadata $class)
	{
		parent::__construct($em, $class);
	}

	/**
     * {@inheritdoc}
     */
    public function findContest(int $id): Contest
    {
        try {
            $article = $this->_em->createQueryBuilder()
                ->select('contest')
                ->from(Contest::class, 'contest')
                ->andWhere('contest.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $article) {
                return $article;
            }

        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findContests(): array
    {
        return $this->_em->getRepository(Contest::class)->findBy(['deleted' => false]);
    }
}
