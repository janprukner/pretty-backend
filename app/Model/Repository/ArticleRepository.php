<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Article;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class ArticleRepository
 * @package App\Model\Repository
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArticleRepository extends BaseRepository implements ArticleRepositoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function findArticle(int $id): Article
    {
        try {
            $article = $this->_em->createQueryBuilder()
                ->select('article')
                ->from(Article::class, 'article')
                ->andWhere('article.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $article) {
                return $article;
            }

        } catch (NonUniqueResultException $e) {

        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findArticles(): array
    {
        return $this->_em->createQueryBuilder()
            ->select('article.id', 'article.key', 'article.text', 'UNIX_TIMESTAMP(article.timestamp) timestamp')
            ->from(Article::class, 'article')
            ->getQuery()
            ->getResult();
    }
}
