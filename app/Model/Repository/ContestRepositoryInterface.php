<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Contest;

/**
 * Interface ContestRepositoryInterface
 * @package App\Model\Repository
 */
interface ContestRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param int $id
     * @return Contest
     * @throws EntityNotFoundException
     */
    public function findContest(int $id): Contest;

    /**
     * @return array
     */
    public function findContests(): array;
}
