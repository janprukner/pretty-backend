<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface RoleRepositoryInterface
 * @package App\Model\Repository
 */
interface RoleRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @return mixed
     */
    public function findDefaultRole();
}
