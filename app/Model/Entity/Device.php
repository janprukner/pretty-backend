<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Device
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity()
 * @ORM\Table(name="device__device")
 */
final class Device
{

    const ANDROID = 'android';
    const ANDROID_TEST = 'android-test';
    const IOS = 'ios';
    const IOS_TEST = 'ios-test';

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $token;

    /**
     * @var string
     * @ORM\Column(name="platform", type="string")
     */
    private $platform = '';

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * Device constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @return array
     */
    public static function getPlatforms(): array
    {
        return [
            self::ANDROID,
            self::ANDROID_TEST,
            self::IOS,
            self::IOS_TEST,
        ];
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform(string $platform): void
    {
        $this->platform = $platform;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTimestamp(): \DateTimeInterface
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTimeInterface $timestamp
     */
    public function setTimestamp(\DateTimeInterface $timestamp): void
    {
        $this->timestamp = $timestamp;
    }


}
