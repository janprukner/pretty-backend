<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 27. 02. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Notification
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="notification__notification", indexes={@ORM\Index(columns={"state"})})
 */
final class Notification
{

    const STATE_ERROR = 'ERROR';
    const STATE_NEW = 'NEW';
    const STATE_OK = 'OK';

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';

    /**
     * @var string
     * @ORM\Column(name="page")
     */
    private $page = '';

    /**
     * @var null|string
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    private $result;

    /**
     * @var string
     * @ORM\Column(name="state")
     */
    private $state = self::STATE_NEW;

    /**
     * @var string
     * @ORM\Column(name="title")
     */
    private $title = '';

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * Model constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->updated = new \DateTime();
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Notification
     */
    public function setDescription(string $description): Notification
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Notification
     */
    public function setId(int $id): Notification
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPage(): string
    {
        return $this->page;
    }

    /**
     * @param string $page
     *
     * @return Notification
     */
    public function setPage(string $page): Notification
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @param string $result
     *
     * @return Notification
     */
    public function setResult(string $result): Notification
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return Notification
     */
    public function setState(string $state): Notification
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle(string $title): Notification
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @param \DateTimeInterface $updated
     *
     * @return Notification
     */
    public function setUpdated(\DateTimeInterface $updated): Notification
    {
        $this->updated = $updated;
        return $this;
    }

}
