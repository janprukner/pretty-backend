<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class Contest
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity()
 * @ORM\Table(name="contest__contest")
 * @ORM\HasLifecycleCallbacks()
 */
final class Contest
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="finish_on", type="datetime")
	 */
    private $finishOn;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime")
	 */
	private $created;

	/**
	 * @var string
	 * @ORM\Column(name="status")
	 */
    private $status;

	/**
	 * @var string
	 * @ORM\Column(name="contest_type")
	 */
    private $contestType;

	/**
	 * @var string
	 * @ORM\Column(name="question")
	 */
    private $question;

	/**
	 * @var string||null
	 * @ORM\Column(name="image")
	 */
    private $image;

	/**
	 * @var PersistentCollection||null
	 * @ORM\OneToMany(targetEntity="App\Model\Entity\Answers", mappedBy="contest", cascade={"remove"}, orphanRemoval=true)
	 */
    private $answers;

	/**
	 * @var PersistentCollection||null
	 * @ORM\OneToMany(targetEntity="App\Model\Entity\User", mappedBy="contest")
	 */
    private $users;

        /**
         * @var boolean
         * @ORM\Column(name="deleted", type="boolean")
         */
    private $deleted = false;


    private $timestamp;

    /**
     * Contest constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime();
        $this->answers = new ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     * @param PreUpdateEventArgs $event
     * @throws \Exception
     */
    public function checkEntityChangeSet(PreUpdateEventArgs $event): void
    {
        if ($event->hasChangedField('order') ||
            $event->hasChangedField('active') ||
            $event->hasChangedField('contestUrl')) {
            $this->timestamp = new \DateTime();
        }
    }

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status): void
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getContestType(): string
	{
		return $this->contestType;
	}

	/**
	 * @param string $contestType
	 */
	public function setContestType(string $contestType): void
	{
		$this->contestType = $contestType;
	}

	/**
	 * @return string
	 */
	public function getQuestion(): string
	{
		return $this->question;
	}

	/**
	 * @param string $question
	 */
	public function setQuestion(string $question): void
	{
		$this->question = $question;
	}

	/**
	 * @return string
	 */
	public function getImage(): string
	{
		return $this->image;
	}

	/**
	 * @param string $image
	 */
	public function setImage(string $image): void
	{
		$this->image = $image;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated(\DateTime $created): void
	{
		$this->created = $created;
	}

	/**
	 * @return \DateTime
	 */
	public function getFinishOn(): \DateTime
	{
		return $this->finishOn;
	}

	/**
	 * @param \DateTime $finishOn
	 */
	public function setFinishOn(\DateTime $finishOn): void
	{
		$this->finishOn = $finishOn;
	}

	/**
	 * @return PersistentCollection||null
	 */
	public function getAnswers(): ?PersistentCollection
	{
		return $this->answers;
	}

	/**
	 * @param PersistentCollection $answers
	 */
	public function setAnswers(PersistentCollection $answers): void
	{
		$this->answers = $answers;
	}

	/**
	 * @return PersistentCollection
	 */
	public function getUsers(): PersistentCollection
	{
		return $this->users;
	}

	/**
	 * @param PersistentCollection $users
	 */
	public function setUsers(PersistentCollection $users): void
	{
		$this->users = $users;
	}

        /**
         * @return bool
         */
        public function isDeleted()
        {
            return $this->deleted;
        }

        /**
         * @param bool $deleted
         */
        public function setDeleted($deleted)
        {
            $this->deleted = (bool)$deleted;
        }


}
