<?php


namespace App\Model\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Answers
 * @package App\Model\Entity
 * @author Roman Havránek
 * @ORM\Entity()
 * @ORM\Table(name="`answers`")
 */
final class Answers
{
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 */
	private $id;

	/**
	 * @var integer
	 * @ORM\Column(name="answer_order", type="integer")
	 */
	private $order;

	/**
	 * @var Contest
	 *
	 * @ORM\ManyToOne(targetEntity="App\Model\Entity\Contest", inversedBy="answers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="contest_id", referencedColumnName="id", onDelete="CASCADE")
	 * })
	 */
	private $contest;

	/**
	 * @var string
	 * @ORM\Column(name="text", type="string")
	 */
	private $text;

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}

	/**
	 * @param int $order
	 */
	public function setOrder(int $order): void
	{
		$this->order = $order;
	}

	/**
	 * @return Contest
	 */
	public function getContest(): Contest
	{
		return $this->contest;
	}

	/**
	 * @param Contest $contest
	 */
	public function setContest(Contest $contest): void
	{
		$this->contest = $contest;
	}

	/**
	 * @return string
	 */
	public function getText(): string
	{
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText(string $text): void
	{
		$this->text = $text;
	}


}
