<?php


namespace App\Model\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserAnswer
 * @package App\Model\Entity
 * @author Roman Havránek
 * @ORM\Entity()
 * @ORM\Table(name="`user_answers`")
 */
final class UserAnswer
{
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(name="answer", type="string")
	 */
	private $answer;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="App\Model\Entity\User", inversedBy="answers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
	private $user;

	/**
	 * @return string
	 */
	public function getAnswer(): string
	{
		return $this->answer;
	}

	/**
	 * @param string $answer
	 */
	public function setAnswer(string $answer): void
	{
		$this->answer = $answer;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser(User $user): void
	{
		$this->user = $user;
	}
}
