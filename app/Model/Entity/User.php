<?php


namespace App\Model\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Class User
 * @package App\Model\Entity
 * @author Roman Havránek
 * @ORM\Entity()
 * @ORM\Table(name="`users`")
 */
final class User
{
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 */
	private $id;

	/**
	 * @var \DateTimeInterface
	 * @ORM\Column(name="timestamp", type="datetime")
	 */
	private $timestamp;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string")
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(name="email", type="string")
	 */
	private $email;

	/**
	 * @var Contest
	 *
	 * @ORM\ManyToOne(targetEntity="App\Model\Entity\Contest", inversedBy="users")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="contest_id", referencedColumnName="id")
	 * })
	 */
	private $contest;

	/**
	 * @var PersistentCollection||null
	 * @ORM\OneToMany(targetEntity="App\Model\Entity\UserAnswer", mappedBy="user")
	 */
	private $answers;

	/**
	 * User constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		$this->timestamp = new \DateTime();
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email): void
	{
		$this->email = $email;
	}

	/**
	 * @return PersistentCollection
	 */
	public function getAnswers(): PersistentCollection
	{
		return $this->answers;
	}

	/**
	 * @param PersistentCollection $answers
	 */
	public function setAnswers(PersistentCollection $answers): void
	{
		$this->answers = $answers;
	}

	/**
	 * @return Contest
	 */
	public function getContest(): Contest
	{
		return $this->contest;
	}

	/**
	 * @param Contest $contest
	 */
	public function setContest(Contest $contest): void
	{
		$this->contest = $contest;
	}

	/**
	 * @return \DateTimeInterface
	 */
	public function getTimestamp(): \DateTimeInterface
	{
		return $this->timestamp;
	}
}
