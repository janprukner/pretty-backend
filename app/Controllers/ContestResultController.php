<?php


namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Contest;
use App\Model\Repository\ContestRepository;
use App\Model\Repository\ContestRepositoryInterface;
use App\Model\ValueObject\ContestValueObject;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineExtensions\Query\Mysql\Date;
use Nette\Utils\DateTime;
use Slim\Http\Request;
use Slim\Http\Response;


final class ContestResultController extends DefaultController
{
	/**
	 * @var ContestRepository
	 */
	private $contestRepository;

	public function __construct(ContestRepositoryInterface $contestRepository)
	{
		$this->contestRepository = $contestRepository;
	}

	public function defaultAction(Request $request, Response $response, array $args)
	{
		if (is_array($args) && isset($args['id'])) {
			try {
				$entity = $this->contestRepository->findContest(intval($args['id']));
				$contestObject = new ContestValueObject($entity);

				return $response->withJson([
					'id' => $contestObject->id,
					'timestamp' => $contestObject->timestamp,
					'name' => 'null',
					'email' => 'null',
					'answers' => $contestObject->answers,
				], 200);

			} catch (EntityNotFoundException $e) {
				return $response->withStatus(404, 'Soutez nenalezena!');
			}
		}

		return $response->withStatus(400);
	}
}
