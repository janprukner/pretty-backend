<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 27. 02. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Repository\NotificationRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class NotificationsController
 * @package App\Controllers
 */
final class NotificationsController extends DefaultController
{

    /**
     * @var NotificationRepositoryInterface
     */
    private $notificationRepository;

    /**
     * NotificationsController constructor.
     *
     * @param NotificationRepositoryInterface $notificationRepository
     */
    public function __construct(NotificationRepositoryInterface $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        return $response->withJson($this->notificationRepository->getAllNotifications(), 200);
    }
}
