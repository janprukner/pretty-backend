<?php


namespace App\Controllers;


use App\Model\Entity\Contest;
use App\Model\Entity\User;
use App\Model\Entity\UserAnswer;
use App\Model\Repository\ContestRepositoryInterface;
use App\Model\ValueObject\ContestValueObject;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Nette\Utils\DateTime;
use Slim\Http\Request;
use Slim\Http\Response;

class ContestActualController extends DefaultController
{

	/**
	 * @var ContestRepositoryInterface
	 */
	private $contestRepository;

	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(ContestRepositoryInterface $contestRepository, EntityManager $em)
	{
		$this->contestRepository = $contestRepository;
		$this->em = $em;
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param array $args
	 * @return Response
	 */
	public function defaultAction(Request $request, Response $response, array $args)
	{
		try {
			// nejprve ukonci souteze, co mely davno skoncit
			$this->checkActualContest();

			$contests = $this->contestRepository->findContests();

			$now = new DateTime();

			$responseArray = [];

			foreach ($contests as $contest){
				if ($now < $contest->getFinishOn() && $contest->getStatus() === 'running'){
					$responseArray[] = new ContestValueObject($contest);
				}
			}

			if (count($responseArray) === 0){
				return $response->withStatus(404, 'Soutez neni aktivni!');
			}

			return $response->withJson($responseArray[0], 200);

		} catch (EntityNotFoundException $e) {
			return $response->withStatus(404, 'Soutez nenalezena!');
		}
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param array $args
	 * @return Response
	 */
	public function resultsAction(Request $request, Response $response, array $args): Response
	{
		try {
			// nejprve ukonci souteze, co mely davno skoncit
			$this->checkActualContest();

			$data = $request->getParsedBody();

			/** @var Contest $contests */
			$contests = $this->contestRepository->findContests();

			$now = new DateTime();
			$actualContest = [];

			/** @var Contest $contest */
			foreach ($contests as $contest){
				if ($now < $contest->getFinishOn() && $contest->getStatus() === 'running'){
					$actualContest = $contest;
					break;
				}
			}

			/** @var Contest $runningContest */
			$runningContest = $this->em->getRepository(Contest::class)->findOneBy(['id' => $contest->getId()]);

			/** @var User $user */
			foreach ($runningContest->getUsers() as $user)
			{
				if ($user->getEmail() === $data['email'])
				{
					return $response->withStatus(409, 'Nelze hlasovat znovu');
				}
			}

			$user = new User();
			$user->setName($data['name']);
			$user->setEmail($data['email']);
			$user->setContest($runningContest);

			foreach ($data['answers'] as $userAnswer){
				$userAnswerEntity = new UserAnswer();
				$userAnswerEntity->setAnswer($userAnswer);
				$userAnswerEntity->setUser($user);
				$this->em->persist($userAnswerEntity);
			}


			$this->em->persist($user);
			$this->em->flush();


			if ($actualContest === []){
				return $response->withStatus(404, 'Soutez neni aktivni!');
			}

			return $response->withStatus(200);

		} catch (EntityNotFoundException $e) {
			return $response->withStatus(404, 'Soutez nenalezena!');
		} catch (ORMException $e) {

		}
	}

	public function checkActualContest(): void
	{
		try {
			/** @var Contest $contests */
			$contests = $this->contestRepository->findContests();

			$now = new \DateTime();

			/** @var Contest $contest */
			foreach ($contests as $contest){
				if ($now->getTimestamp() >= $contest->getFinishOn()->getTimestamp()){
					if ($contest->getStatus() === 'running' || $contest->getStatus() === 'paused'){
						$contest->setStatus('finished');
					}
				}
			}

			$this->em->flush($contests);
		} catch (OptimisticLockException $e) {

		} catch (ORMException $e) {

		}
	}

}
