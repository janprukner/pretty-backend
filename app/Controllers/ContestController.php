<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Contest;
use App\Model\Entity\User;
use App\Model\Repository\ContestRepositoryInterface;
use App\Model\ValueObject\ContestValueObject;
use Doctrine\ORM\EntityManager;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContestController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestController extends DefaultController
{

    /**
     * @var ContestRepositoryInterface
     */
    private $contestRepository;

    /**
	 * @var EntityManager
	 */
	private $em;

	/**
     * ContestController constructor.
     * @param ContestRepositoryInterface $contestRepository
     */
    public function __construct(ContestRepositoryInterface $contestRepository, EntityManager $em)
    {
        $this->contestRepository = $contestRepository;
		$this->em = $em;
	}

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->contestRepository->findContest(intval($args['id']));
                $contestValueObject = new ContestValueObject($entity);
                $contestValueObject->timestamp = $contestValueObject->timestamp->getTimestamp();
                $contestValueObject->finishOn = $contestValueObject->finishOn->getTimestamp();
                $contestValueObject->created = $contestValueObject->created->getTimestamp();
                
                return $response->withJson($contestValueObject, 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

	/**
	 * @param Request $request
	 * @param Response $response
	 * @param array $args
	 * @return Response
	 */
	public function resultsAction(Request $request, Response $response, array $args)
	{
		if (is_array($args) && isset($args['id'])) {
			try {

				/** @var Contest $contest */
				$contest = $this->em->getRepository(Contest::class)->findOneBy(['id' => $args['id']]);

				if ($contest){
					$users = $contest->getUsers();

					$userAnswers = [];
					foreach ($users as $user)
					{
						$userOnlyAnswers = [];

						foreach ($user->getAnswers() as $userAnswer){
							$userOnlyAnswers[] = $userAnswer->getAnswer();
						}

						$userAnswers[] = [
							'id' => $user->getId(),
							'timestamp' => $user->getTimestamp()->getTimestamp(),
							'name' => $user->getName(),
							'email' => $user->getEmail(),
							'answers' => $userOnlyAnswers
						];
					}

					return $response->withJson($userAnswers);
				}

				return $response->withStatus(404);


			} catch (EntityNotFoundException $e) {
				return $response->withStatus(404);
			}
		}

		return $response->withStatus(400);
	}
}
