<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use Psr\Container\ContainerInterface;

/**
 * Class DefaultController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
abstract class DefaultController
{
	protected $statusAllowedTypes = [
		'paused', 'running', 'finished'
	];

	protected $contestAllowedTypes = [
		'text', 'rating', 'voting'
	];

    /**
     * @var ContainerInterface $container
     */
    protected $container;
}
