<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Contest;
use App\Model\Repository\ContestRepositoryInterface;
use App\Model\ValueObject\ContestValueObject;
use Slim\Http\Request;
use Slim\Http\Response;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class ContestsController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestsController extends DefaultController
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContestRepositoryInterface
     */
    private $contestRepository;

    /**
     * ContestsController constructor.
     * @param ContestRepositoryInterface $contestRepository
     */
    public function __construct(ContestRepositoryInterface $contestRepository, EntityManager $em)
    {
        $this->contestRepository = $contestRepository;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
	// kontrola, jestli soutez nevyprsela
	$this->checkActualContest();

        /** @var Contest $contests */
		$contests = $this->contestRepository->findContests();
		
		
		// dostal jsem souteze a ted vycistim format, odstranim nepotrebne veci
    	$contestsArray = [];
    	foreach ($contests as $contest)
		{
		    $contestValueObject = new ContestValueObject($contest);
		    $contestValueObject->timestamp = $contestValueObject->timestamp->getTimestamp();
		    $contestValueObject->finishOn = $contestValueObject->finishOn->getTimestamp();
		    $contestValueObject->created = $contestValueObject->created->getTimestamp();
		    $contestsArray[] = $contestValueObject;
		}

        return $response->withJson($contestsArray, 200);
        
    }

        public function checkActualContest(): void
        {
                try {
                        /** @var Contest $contests */
                        $contests = $this->contestRepository->findContests();

                        $now = new \DateTime();

                        /** @var Contest $contest */
                        foreach ($contests as $contest){
                                if ($now->getTimestamp() >= $contest->getFinishOn()->getTimestamp()){
                                        if ($contest->getStatus() === 'running' || $contest->getStatus() === 'paused'){
                                                $contest->setStatus('finished');
                                        }
                                }
                        }

                        $this->em->flush($contests);
                } catch (OptimisticLockException $e) {

                } catch (ORMException $e) {

                }
        }

}
