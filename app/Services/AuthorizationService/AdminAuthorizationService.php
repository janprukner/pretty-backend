<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\AuthorizationService;

use App\Constants\Roles;
use App\Model\Entity\TokenEntity;
use App\Services\TokenStorage\TokenStorageInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class AuthenticationService
 * @package App\Services\AuthorizationService
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class AdminAuthorizationService
{

    /** @var TokenStorageInterface $tokenStorage */
    private $tokenStorage;

    /**
     * AdminAuthorizationService constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param mixed $next
     * @return mixed
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response, $next)
    {
        /** @var TokenEntity $token */
        $token = $this->tokenStorage->getToken();

        if ($token instanceof TokenEntity) {
            $user = $token->getUser();
            if ($user->getRole()->getId() === Roles::ROLE_ADMIN) {
                return $next($request, $response);
            }
        }

        return $response->withStatus(401);
    }

}
