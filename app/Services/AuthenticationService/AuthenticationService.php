<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\AuthenticationService;

use App\Exceptions\InvalidAuthTokenException;
use App\Model\Entity\TokenEntity;
use App\Model\Repository\TokenRepositoryInterface;
use App\Services\TokenStorage\TokenStorageInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AuthenticationService
 * @package App\Services\AuthenticationService
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class AuthenticationService
{

    /**
     * @var TokenRepositoryInterface
     */
    private $tokenRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AuthenticationService constructor.
     * @param TokenRepositoryInterface $tokenRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenRepositoryInterface $tokenRepository,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->tokenRepository = $tokenRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param mixed $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        try {
            if ($token = $this->getBearerToken($request)) {
                $tokenEntity = $this->tokenRepository->verifyToken($token);
                if ($tokenEntity instanceof TokenEntity) {
                    $this->tokenStorage->setToken($tokenEntity);
                    return $next($request, $response);
                }
            }
        } catch (InvalidAuthTokenException $e) {

        }

        return $response->withStatus(401);
    }

    /**
     * @param Request $request
     * @return null|string
     */
    function getBearerToken(Request $request)
    {
        $pattern = '/Bearer\s(\S+)/';
        if ($headers = $request->getHeaderLine('Authorization')) {
            if (preg_match($pattern, $headers, $matches)) {
                return $matches[1];
            }
        }

        if ($headers = $request->getHeaderLine('authorization')) {
            if (preg_match($pattern, $headers, $matches)) {
                return $matches[1];
            }
        }

        if ($headers = $request->getHeaderLine('x-authorization')) {
            if (preg_match($pattern, $headers, $matches)) {
                return $matches[1];
            }
        }

        return null;
    }
}
