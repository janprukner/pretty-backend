<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\TokenStorage;

use App\Model\Entity\TokenEntity;

/**
 * Interface TokenStorageInterface
 * @package App\Services\TokenStorage
 */
interface TokenStorageInterface
{

    /**
     * @param TokenEntity $token
     * @return mixed
     */
    public function setToken(TokenEntity $token);

    /**
     * @return TokenEntity
     */
    public function getToken();
}
