<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\MailService;

/**
 * Interface MailService
 * @package App\Services\MailService
 */
interface MailServiceInterface
{

    /**
     * @param mixed $address
     * @param mixed $subject
     * @param mixed $template
     * @param array $data
     * @return bool
     */
    public function sendTemplate($address, $subject, $template, array $data = []);
}
